﻿using System.Web.Script.Serialization;
using System;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.Win32;
using JsonConfig;
using System.IO;
using LijnMonitor.Properties;

namespace LijnMonitor
{
    public partial class WachtrijForm : Form
    {
        public Dictionary<string, string> replace;

        WebClient webClient;

        public WachtrijForm()
        {
            InitializeComponent();
            if (File.Exists(".\\settings.conf")) { File.WriteAllBytes(".\\settings.conf", Resources.settings); }
            if (!File.Exists(".\\settings.conf")) { File.WriteAllBytes(".\\settings.conf", Resources.settings); }
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;

            IntervalTimer.Start();
            
            SystemEvents.SessionEnding += SystemEvents_SessionEnding;

            webClient = new WebClient();
            webClient.DownloadStringCompleted += WebClient_DownloadStringCompleted;
            
            SetStartup();
            getData();

        }

        void sizeDGV(DataGridView dgv)
        {
            DataGridViewElementStates states = DataGridViewElementStates.None;
            dgv.ScrollBars = ScrollBars.None;
            var totalHeight = dgv.Rows.GetRowsHeight(states) + dgv.ColumnHeadersHeight;
            totalHeight += dgv.Rows.Count * 4; 
            var totalWidth = dgv.Columns.GetColumnsWidth(states) + dgv.RowHeadersWidth;
            dgv.ClientSize = new Size(totalWidth, totalHeight);

            int totalWidth2 = 0;
            foreach (DataGridViewColumn col in dataGridView1.Columns)
                totalWidth2 += col.Width;

            this.Width = totalWidth2;

            int totalHeight2 = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
                totalHeight2 += row.Height;


            this.Height = totalHeight2 + 56;

        }


        private void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            FormClosingEventArgs ea = new FormClosingEventArgs(CloseReason.WindowsShutDown, false);
            LijnInfo_FormClosing(sender, ea);
        }

        private void SetStartup()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk.SetValue("LijnMonitor", Application.ExecutablePath.ToString());
        }

        public void getData()
        {
            try {
              webClient.DownloadStringAsync(new Uri("http://feed.infomedics.nl/service.svc/data"));
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var jss = new JavaScriptSerializer();
            LijnData data = jss.Deserialize<LijnData>(e.Result);

            List<ResultData> rd = this.makeList(data);

            
            dataGridView1.DataSource = rd;
           
            sizeDGV(dataGridView1);

        }

        private List<ResultData> makeList(LijnData reponse)
        {
            try {
                List<ResultData> rd = new List<ResultData>();
                    foreach (dynamic lijn in Config.Global.Lijnen)
                    {
               
                    var result = (from x in reponse.lijnen where lijn.id == x.naam select new ResultData{ Naam = lijn.shortname, Wachtrij = x.wachtrij, Wachttijd = x.wachttijd, Gemiddelde = x.avgwachttijd });

                    foreach (ResultData rds in result) {
                        rd.Add(rds);
                    }
                    }
                return rd;
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return new List<ResultData>();
            }
        }

        private void IntervalTimer_Tick(object sender, EventArgs e)
        {
            getData();
        }

        private void LijnInfo_FormClosing(object sender, FormClosingEventArgs e)
        {

            closeProgram(e);
        }

        public static void closeProgram(FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                Application.Exit();
            }
            else if (e.CloseReason == CloseReason.ApplicationExitCall)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
         
            dataGridView1.ClearSelection();
            
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToInt16(row.Cells[1].Value) > 0)
                {
                    row.DefaultCellStyle.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = System.Drawing.Color.White;
                }
            }
        }

        private void WachtrijForm_LocationChanged(object sender, EventArgs e)
        {
            Form f = (Form)sender;
            

            if (!IsOnScreen(f))
            {
                var xy = Screen.FromPoint(Cursor.Position).Bounds;
                xy.X = xy.X + xy.Width -Form.ActiveForm.Size.Width;
                xy.Y = xy.Y + xy.Height - Form.ActiveForm.Size.Height;
                Form.ActiveForm.Location = xy.Location;
                //Form.ActiveForm.Location = new Point(xy.Width - Form.ActiveForm.Width, xy.Height - Form.ActiveForm.Height);
            }
            
        }

        private void KeepBounds()
        {
            if (this.Left < SystemInformation.VirtualScreen.Left)
                this.Left = SystemInformation.VirtualScreen.Left;

            if (this.Right > SystemInformation.VirtualScreen.Right)
                this.Left = SystemInformation.VirtualScreen.Right - this.Width;

            if (this.Top < SystemInformation.VirtualScreen.Top)
                this.Top = SystemInformation.VirtualScreen.Top;

            if (this.Bottom > SystemInformation.VirtualScreen.Bottom)
                this.Top = SystemInformation.VirtualScreen.Bottom - this.Height;
        }

        private void WachtrijForm_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Form f = new Password();
            f.ShowDialog();
        }

        public bool IsOnScreen(Form form)
        {
            Screen[] screens = Screen.AllScreens;
            foreach (Screen screen in screens)
            {
                Rectangle formRectangle = new Rectangle(form.Left, form.Top,
                                                         form.Width, form.Height);

                if (screen.WorkingArea.Contains(formRectangle))
                {
                    return true;
                }
            }

            return false;
        }

        private void WachtrijForm_Move(object sender, EventArgs e)
        {
            KeepBounds();
        }
    }
}
