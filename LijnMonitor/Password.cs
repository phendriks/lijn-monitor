﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LijnMonitor
{
    public partial class Password : Form
    {
        public Password()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkPassword();
        }

        private void checkPassword()
        {
            if (textBox1.Text == "Supergeheim")
            {
                Form f = new Settings();
                f.FormClosed += F_FormClosed;
                f.Show();
                this.Hide();
            }
            else
            {
                ActiveForm.Close();
            }
        }

        private void F_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                checkPassword();
            }
        }
    }
}
