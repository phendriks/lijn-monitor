﻿namespace LijnMonitor
{
    public class LijnData
    {
        public string datetimestamp { get; set; }
        public Lijn[] lijnen {get; set; }
    }
    public class Lijn
    {
        public string avgwachttijd { get; set; }
        public int callshandled { get; set; }
        public Gebruiker gebruikers { get; set; }
        public string naam { get; set; }
        public int wachtrij { get; set; }
        public string wachttijd { get; set; }
    }
    public class Gebruiker
    {
        public int ingelogd { get; set; }
        public int ingesprek { get; set; }
        public int ready { get; set; }
    }
}


public class ResultData
{
    public string Naam { get; set; }
    public int Wachtrij { get; set; }
    public string Wachttijd { get; set; }
    public string Gemiddelde { get; set; }
}