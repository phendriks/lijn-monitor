﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LijnMonitor
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormClosingEventArgs fc = new FormClosingEventArgs(CloseReason.ApplicationExitCall, false);
            LijnMonitor.WachtrijForm.closeProgram(fc);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk.DeleteValue("LijnMonitor");
            MessageBox.Show("Auto start is uitgeschakeld.");
        }
    }
}
